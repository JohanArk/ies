import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

class CustomImage
{
    ImageView imageView = null;

    CustomImage(int type, int ratio)
    {
        String imgSource = getImageSource(type);

        Image defaultImage = new Image(imgSource);

        double width = defaultImage.getWidth() * ratio;
        double height = defaultImage.getHeight() * ratio;

        Image scaledImage = new Image(imgSource, width, height, false, false);

        imageView = new ImageView(scaledImage);
    }

    private String getImageSource(int type)
    {
        switch (type)
        {
            case -3:
                return "img/tileColor.png";
            case -2:
                return "img/tileHover.png";
            case -1:
                return "img/tileEmpty.png";
            case 1:
                return  "img/tileGrass.png";
            case 2:
                return  "img/tileFlowerGrass.png";
            case 3:
                return  "img/tileSand.png";
            case 4:
                return  "img/tileLava.png";
            case 5:
                return  "img/tileWater.png";
            case 6:
                return  "img/tileSnow.png";
            case 7:
                return  "img/tileStone.png";
            case 8:
                return  "img/tree1.png";
            case 9:
                return  "img/player.png";
            default:
                return "img/tileGrass.png";
        }
    }
}
