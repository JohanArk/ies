import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

class Tile
{
    private Pane tilePane;
    private Pane floorPane;
    private Pane objectPane;
    private Pane playerPane;

    Tile(int type, int ratio)
    {
        tilePane = new Pane();
        floorPane = new Pane();
        objectPane = new Pane();
        playerPane = new Pane();

        tilePane.getChildren().addAll(floorPane, objectPane, playerPane);

        floorPane.getChildren().add(new CustomImage(type, ratio).imageView);
    }

    void updateImage(int aType, int ratio)
    {
        floorPane.getChildren().clear();

        floorPane.getChildren().add(new CustomImage(aType, ratio).imageView);
    }

    Pane getPane()
    {
        return tilePane;
    }
    Pane getObjectPane()
    {
        return objectPane;
    }

    void addPlayer(int ratio)
    {
        CustomImage playerImage = new CustomImage(9, ratio);

        double imageHeight = playerImage.imageView.getImage().getHeight();
        double layoutY = floorPane.getLayoutY() - (imageHeight/2);

        playerPane.setLayoutY(layoutY);
        playerPane.getChildren().add(playerImage.imageView);
    }

    void removePlayer()
    {
        playerPane.getChildren().clear();
    }

    void addObject(int aType, int aRatio)
    {
        // Only allows one object on each tile
        objectPane.getChildren().clear();

        CustomImage image = new CustomImage(aType, aRatio);

        double imageHeight = image.imageView.getImage().getHeight();
        double layoutY = floorPane.getLayoutY() - (imageHeight/2);

        objectPane.setLayoutY(layoutY);
        objectPane.getChildren().add(image.imageView);
    }
}
