// Used for the Application.
import com.sun.javafx.geom.Vec2d;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

// Used to get the screen resolution.
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Random;

public class Game extends Application
{
    private int ratio = 3;

    // Add world size
    private Vec2d vWorldSize = new Vec2d(10.0, 10.0);
    private Vec2d vTileSize = new Vec2d(40 * ratio, 20 * ratio);
    private Vec2d vOrigin = new Vec2d(5, 1);
    private Vec2d vPlayer;
//    private Image[] sprites = null;
    private Tile[][] aWorld = null;

    // Labels
    private Label lMouse, lCell, lSelected, lTileType, lPlayer;

    private Tile selectedTile = null;
    private int selectedTileType = 1;


    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        Pane mainPane = new Pane();
        Pane tempPane = new Pane();
        Pane tilesPane = new Pane();
        VBox labelsPane = new VBox();

        mainPane.getChildren().addAll(tilesPane, tempPane, labelsPane);

        initWorld(tilesPane);
        generateRandom();
        initPlayer();
        initLabels(labelsPane);


        primaryStage.addEventFilter(KeyEvent.KEY_PRESSED, key ->
        {
            if (key.getCode() == KeyCode.W || key.getCode() == KeyCode.A || key.getCode() == KeyCode.S || key.getCode() == KeyCode.D)
            {
                movePlayer(key.getCode());
            }
        });


        // TODO: Load images from this method.
//        addSprites();

        mainPane.setOnMouseClicked(event ->
        {
            // Left-click
            if (event.getButton().equals(MouseButton.PRIMARY))
            {
                // Add tile
                if (selectedTile != null)
                {
                    if (selectedTileType == 8)
                    {
                        selectedTile.addObject(8, ratio);
                    }
                    else
                    {
                        selectedTile.updateImage(selectedTileType, ratio);
                    }
                }
            }
            // Right-click
            else if (event.getButton().equals(MouseButton.SECONDARY))
            {
                // Select type of tile
                if (selectedTileType >= 8)
                {
                    selectedTileType = 0;
                }
                selectedTileType++;

                lTileType.setText("Tile type: " + selectedTileType);
            }
        });

        mainPane.setOnMouseMoved(event ->
        {
            Vec2d vMouse = new Vec2d(event.getX(), event.getY());

            selectTile(vMouse, tempPane);
        });
        initStage(primaryStage, mainPane);
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void generateRandom()
    {
        Random r = new Random();

        for (int y = 0; y < aWorld.length; y++)
        {
            for (int x = 0; x < aWorld[0].length; x++)
            {
                int floor = r.nextInt(3-1) + 1;
                int tree = r.nextInt(5-1) + 1;

                Tile tile = aWorld[x][y];
                tile.updateImage(floor, ratio);

                if (tree == 1)
                {
                    tile.addObject(8, ratio);

                }
            }
        }
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void movePlayer(KeyCode key)
    {
        if (key == KeyCode.W)
        {
            // Move down X
            if (vPlayer.x > 0)
            {
                aWorld[(int) vPlayer.y][(int) vPlayer.x].removePlayer();
                aWorld[(int) vPlayer.y][(int) vPlayer.x - 1].addPlayer(ratio);

                vPlayer.x--;
            }
        }
        else if (key == KeyCode.S)
        {
            // Move up X
            if (vPlayer.x < vWorldSize.x - 1)
            {
                aWorld[(int) vPlayer.y][(int) vPlayer.x].removePlayer();
                aWorld[(int) vPlayer.y][(int) vPlayer.x + 1].addPlayer(ratio);

                vPlayer.x++;
            }
        }
        else if (key == KeyCode.A)
        {
            // Move up Y
            if (vPlayer.y < vWorldSize.y - 1)
            {
                aWorld[(int) vPlayer.y][(int) vPlayer.x].removePlayer();
                aWorld[(int) vPlayer.y + 1][(int) vPlayer.x].addPlayer(ratio);

                vPlayer.y++;
            }
        }
        else if (key == KeyCode.D)
        {
            // Move down Y
            if (vPlayer.y > 0)
            {
                aWorld[(int) vPlayer.y][(int) vPlayer.x].removePlayer();
                aWorld[(int) vPlayer.y - 1][(int) vPlayer.x].addPlayer(ratio);

                vPlayer.y--;
            }
        }

        lPlayer.setText("Player: " + (int) vPlayer.x + ", " + (int) vPlayer.y);
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void initPlayer()
    {
        Random r = new Random();

        int x = r.nextInt((int) vWorldSize.x);
        int y = r.nextInt((int) vWorldSize.y);

        aWorld[y][x].addPlayer(ratio);
        vPlayer = new Vec2d(x,y);
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void selectTile(Vec2d mouse, Pane tempPane)
    {
        selectedTile = null;
        tempPane.getChildren().clear();

        Vec2d vMouse = new Vec2d(mouse.x, mouse.y);
        Vec2d vCell = new Vec2d((int)(vMouse.x / vTileSize.x), (int)(vMouse.y / vTileSize.y));
        Vec2d vOffset = new Vec2d(vMouse.x % vTileSize.x, vMouse.y % vTileSize.y);

        Vec2d vSelected = new Vec2d
                (
                    (vCell.y - vOrigin.y) + (vCell.x - vOrigin.x),
                    (vCell.y - vOrigin.y) - (vCell.x - vOrigin.x)
                );

        String color = getColor((int) vOffset.x, (int) vOffset.y);

        // Gets the correct corresponding tile.
        if (color.equalsIgnoreCase("red"))
        {
            vSelected.x -= 1;
        }
        else if (color.equalsIgnoreCase("blue"))
        {
            vSelected.y -= 1;
        }
        else if (color.equalsIgnoreCase("green"))
        {
            vSelected.y += 1;
        }
        else if (color.equalsIgnoreCase("yellow"))
        {
            vSelected.x += 1;
        }

        if (vSelected.x >= 0 && vSelected.x < vWorldSize.x)
        {
            if (vSelected.y >= 0 && vSelected.y < vWorldSize.y)
            {
                Vec2d vSelectedWorld = toScreen((int) vSelected.x, (int) vSelected.y);

                selectedTile = aWorld[(int) vSelected.y][(int) vSelected.x];

                drawTile(new Tile(-2, ratio), vSelectedWorld, tempPane);

            }
        }
        updateInfoLabels(vMouse, vCell, vSelected);
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void updateInfoLabels(Vec2d mouse, Vec2d cell, Vec2d selected)
    {
        lMouse.setText("Mouse: " + mouse.x + ", " + mouse.y);
        lCell.setText("Cell: " + (int) cell.x + ", " + (int) cell.y);
        lSelected.setText("Selected: " + (int) selected.x + ", " + (int) selected.y);
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void initStage(Stage primaryStage, Pane node)
    {
        primaryStage.setTitle("Fabeln v0.0.4");

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double windowWidth = screenSize.getWidth() * 9.0 / 10.0;
        double windowHeight = screenSize.getHeight() * 9.0 / 10.0;

        Scene scene = new Scene(node, windowWidth, windowHeight);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void initLabels(Pane node)
    {
        lMouse = new Label("Mouse: ");
        lCell = new Label("Cell: ");
        lSelected = new Label("Selected: ");
        lTileType = new Label("Tile type: " + selectedTileType);
        lPlayer = new Label("Player: " + (int) vPlayer.x + ", " + (int) vPlayer.y);

        node.getChildren().addAll(lMouse, lCell, lSelected, lTileType, lPlayer);
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void initWorld(Pane node)
    {
//        pWorld = new int[(int) (vWorldSize.x * vWorldSize.y)];
        aWorld = new Tile[(int) vWorldSize.y][(int) vWorldSize.x];

        for (int y = 0; y < aWorld.length; y++)
        {
            for (int x = 0; x < aWorld[0].length; x++)
            {
                Vec2d vWorld = toScreen(x, y);

                aWorld[y][x] = new Tile(1, ratio);
                drawTile(aWorld[y][x], vWorld, node);
            }
        }
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private Vec2d toScreen(int x, int y)
    {
        return new Vec2d
        (
            (vOrigin.x * vTileSize.x) + (x - y) * (vTileSize.x / 2),
            (vOrigin.y * vTileSize.y) + (x + y) * (vTileSize.y / 2)
        );
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private void drawTile(Tile tile, Vec2d location, Pane node)
    {
        tile.getPane().setLayoutX((int)location.x);
        tile.getPane().setLayoutY((int)location.y);

        node.getChildren().addAll(tile.getPane());
    }

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    //TODO Make so automatically load sprites from isometric_tiles.png
    /*
    private void addSprites()
    {
        //Image sprites = new Image("isometric_tiles.png");
    }
    */

    //
    //-----------------------------------------------------------------------------------------------------------------------
    //

    private String getColor(int x, int y)
    {
        Image colorImage = new Image("img/tileColor.png", vTileSize.x, vTileSize.y, false, false);

        PixelReader pixelReader = colorImage.getPixelReader();

        int color = pixelReader.getArgb(x, y);

        // gets red, green, blue (rgb) color value.
        int red = (color >> 16) & 0xff;
        int green = (color >> 8) & 0xff;
        int blue = color & 0xff;

        // Returns the color.
        if (red == 0 && green == 38 && blue == 255)
        {
            return "blue";
        }
        else if (red == 0 && green == 255 && blue == 33)
        {
            return "green";
        }
        else if (red == 255 && green == 0 && blue == 0)
        {
            return "red";
        }
        else if (red == 255 && green == 216 && blue == 0)
        {
            return "yellow";
        }
        else if (red == 255 && green == 255 && blue == 255)
        {
            return "white";
        }

        // If no color.
        return "";
    }
}
